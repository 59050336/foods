package com.example.demo.controllers;
import com.example.demo.entities.PersonEntity;
import com.example.demo.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
@RestController
public class PersonController {
@Autowired
private PersonRepository repository;
@RequestMapping("/people")
public List<PersonEntity> findByLastName(@RequestParam(value = "last_name") String lastName) {
return repository.findByLastName(lastName); }
@RequestMapping(value = "/people", method = RequestMethod.POST)
public PersonEntity create(@RequestParam(value = "first_name") String firstName
, @RequestParam(value = "last_name") String lastName) { PersonEntity newPerson = new PersonEntity(); newPerson.setFirstName(firstName); newPerson.setLastName(lastName);
repository.save(newPerson);
return newPerson; }
}