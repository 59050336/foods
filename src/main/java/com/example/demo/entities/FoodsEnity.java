package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class FoodsEnity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String nameF;
    private String calorie;

    public String getnameF() {
        return nameF;
    }

    public void setnameF(String nameF) {
        this.nameF = nameF;
    }

    public String getcalorie() {
        return calorie;
    }

    public void setcalorie(String calorie) {
        this.calorie = calorie;
    }
}