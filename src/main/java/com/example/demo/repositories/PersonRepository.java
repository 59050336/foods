
package com.example.demo.repositories;

import com.example.demo.entities.PersonEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface PersonRepository extends CrudRepository<PersonEntity, Long> {
    List<PersonEntity> findByLastName(@Param("name") String name);
}